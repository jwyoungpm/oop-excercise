from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os


class PGConnect:

    session = None
    engine = None

    def __init__(self):
        postgres_host = os.environ.get("POSTGRES_HOST", "localhost")
        postgres_password = os.environ.get("POSTGRES_PASSWORD", "DontCheckinYourPasswords123")
        postgres_user = os.environ.get("POSTGRES_USER", "postgres")
        postgres_database = os.environ.get("POSTGRES_DATABASE", "postgres")

        engine = create_engine(
            "postgresql://{}:{}@{}/{}".format(
                postgres_user, postgres_password, postgres_host, postgres_database
            )
        )

        self.engine = engine

    def __del__(self):
        if self.session is not None:
            self.session.close()

    def get_session(self):
        Session = sessionmaker(bind=self.engine)

        self.session = Session()

        return self.session

    def commit(self):
        try:
            self.session.commit()
        except:
            self.session.rollback()
            raise
