from src.model.teacher import TeacherModel
from src.utils.pgconnect import PGConnect
from src.schema.teacher import TeacherSchema
from sqlalchemy.exc import IntegrityError


class TeacherController:

    first_name = None
    last_name = None
    id = None
    email = None

    def __init__(self, first_name, last_name, email):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        try:
            self._create_if_not_exists()
        except Exception as e:
            raise e

    def _create_if_not_exists(self):
        pgconnection = PGConnect()
        pgsession = pgconnection.get_session()

        results, errors = TeacherSchema().load(
            {
                "first_name": self.first_name,
                "last_name": self.last_name
            }
        )
        if len(errors) > 0:
            raise ValueError("One or more errors occurred when validating the name: {}".format(str(errors)))

        teacher = TeacherModel(
            first_name=self.first_name,
            last_name=self.last_name,
            email = self.email
        )

        try:
            pgsession.add(teacher)
            pgsession.commit()
            self.id = teacher.id
        except IntegrityError as e:
            teacher = (
                pgsession.query(TeacherModel).filter(Teacher.email == self.email).one()
            )
            self.id = teacher.id
            return True
        except Exception as e:
            raise Exception(e)
