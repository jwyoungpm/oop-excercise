from marshmallow import Schema, fields, ValidationError
import re

def validate_name_length(name):
    if len(name) > 30:
        raise ValidationError("Must be 30 characters or less")

def validate_only_letters(name):
    reg_match = re.compile('^[a-zA-Z]+$')
    if not reg_match.match(name):
        raise ValidationError("Must contain only letters a-z or A-Z")


class TeacherSchema(Schema):
    first_name = fields.Str(validate=[validate_name_length,validate_only_letters])
    last_name = fields.Str(validate=[validate_name_length,validate_only_letters])
    email = fields.Email()
