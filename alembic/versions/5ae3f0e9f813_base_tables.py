"""empty message

Revision ID: 5ae3f0e9f813
Revises:
Create Date: 2019-05-31 19:30:33.997198

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "5ae3f0e9f813"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table("teachers")
    op.create_table("students")
    op.create_table("classes")
    op.create_table("quizzes")


def downgrade():
    pass
