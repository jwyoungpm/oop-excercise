"""empty message

Revision ID: d73ba8cb3bdf
Revises: 5ae3f0e9f813
Create Date: 2019-05-31 19:40:03.906302

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "d73ba8cb3bdf"
down_revision = "5ae3f0e9f813"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "teachers", sa.Column("id", sa.Integer(), primary_key=True, nullable=False)
    )
    op.add_column("teachers", sa.Column("first_name", sa.String(30), nullable=False))
    op.add_column("teachers", sa.Column("last_name", sa.String(30), nullable=False))
    op.add_column("teachers", sa.Column("email", sa.String(), nullable=False))
    op.create_unique_constraint("uq_teacher_email", "teachers", ["email"])


def downgrade():
    pass
