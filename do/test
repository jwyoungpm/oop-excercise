#!/bin/bash +e

export PYTHONPATH="$(pwd)/src"
sh do/clean 2>/dev/null

docker-compose up -d --build

wait_for_postgres(){
  exit=0
  set +e
  while [ $exit -ne 1 ]; do
    docker ps | grep school_postgres | grep start > /dev/null
    exit=$?
  done
  set -e
}

get_postgres_ip(){
  CONTAINER_LABEL=$(docker-compose ps | grep school_postgres | cut -d " " -f1 | tr -d -)
  echo "Found container label $CONTAINER_LABEL"
  NETWORK_LABEL=$(docker inspect $CONTAINER_LABEL | jq '.[0].NetworkSettings.Networks| keys[] ' -r)
  echo "Found network label $NETWORK_LABEL"
  export POSTGRES_HOST=$(docker inspect $CONTAINER_LABEL | jq ".[0].NetworkSettings.Networks.\"$NETWORK_LABEL\".IPAddress" -r)
  if [ "$POSTGRES_HOST" == "" ]; then
    echo "Unable to find postgres host!"
    exit 1
  else
    echo "Found host at $POSTGRES_HOST"
  fi
}

db_init(){
  alembic upgrade head
}

wait_for_postgres
get_postgres_ip
db_init

py.test test/*/test*.py --cov=src --junitxml=coverage.xml
TEST_EXIT=$?

docker-compose down
docker-compose rm

exit $TEST_EXIT
