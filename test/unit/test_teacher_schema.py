import unittest
from src.schema.teacher import TeacherSchema

class TestTeacherSchema(unittest.TestCase):

    def setUp(self):
        pass

    def test_teacher_success(self):
        good_first_name = "James"
        good_last_name = "Young"
        good_email = "jwyoungpm@gmail.com"

        results, errors = TeacherSchema().load(
            {
                "first_name": good_first_name,
                "last_name": good_last_name
            }
        )
        print(results)
        print(errors)
        self.assertTrue(len(errors.keys()) == 0)

    def test_teacher_schema_exceeds_length(self):
        bad_last_name = "LookAtMeImOverThirtyCharactersLong"
        bad_first_name = "MyFirstNameIsAbsurdlyLongAsWell"
        good_email = "jwyoungpm@gmail.com"

        results, errors = TeacherSchema().load(
            {
                "first_name": bad_first_name,
                "last_name": bad_last_name
            }
        )

        self.assertTrue(len(errors.keys()) == 2)
        self.assertTrue(errors["first_name"][0] == "Must be 30 characters or less")
        self.assertTrue(errors["last_name"][0] == "Must be 30 characters or less")

    def test_teacher_schema_invalid_chars(self):
        bad_last_name = "L$ssThan30"
        bad_first_name = "NoNumbersOr Spaces"
        good_email = "jwyoungpm@gmail.com"

        results, errors = TeacherSchema().load(
            {
            "first_name": bad_first_name,
            "last_name": bad_last_name
            }
        )

        self.assertTrue(len(errors.keys()) == 2)
        self.assertTrue(len(errors['first_name']) == 1)
        self.assertTrue(len(errors['last_name']) == 1)
        self.assertTrue("Must contain only letters a-z or A-Z" in errors['first_name'])
        self.assertTrue("Must contain only letters a-z or A-Z" in errors['last_name'])

    def tearDown(self):
        pass
