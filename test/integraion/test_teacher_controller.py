import unittest
from src.controller.teacher import TeacherController
from src.model.teacher import TeacherModel
from src.utils.pgconnect import PGConnect

class TestTeacherController(unittest.TestCase):

    def setUp(self):
        pass

    def test_teacher_successful_add(self):
        first_name = "James"
        last_name = "Young"
        email = "jwyoungpm@gmail.com"

        teacher_james = TeacherController(first_name, last_name, email)

        pgconnection = PGConnect()
        pgsession = pgconnection.get_session()

        teacher_from_db = (
            pgsession.query(TeacherModel).filter(TeacherModel.first_name == first_name, TeacherModel.last_name == last_name).one()
        )

        pgsession.delete(teacher_from_db)
        pgsession.commit()

        print(teacher_from_db.id)
        print(teacher_james.id)

        self.assertTrue(teacher_from_db.email == email)
        self.assertTrue(teacher_from_db.id == teacher_james.id)

    def tearDown(self):
        pass
