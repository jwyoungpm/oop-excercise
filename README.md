[![pipeline status](https://gitlab.com/jwyoungpm/oop-excercise/badges/master/pipeline.svg)](https://gitlab.com/jwyoungpm/oop-excercise/commits/master)
[![coverage report](https://gitlab.com/jwyoungpm/oop-excercise/badges/master/coverage.svg)](https://gitlab.com/jwyoungpm/oop-excercise/commits/master)

# Setup

This project you have:
* python3.6+ + pip
* virtualenv
* docker
* docker-compose

To get setup, run `source do/setup`.
This will create a virtualenv and install everything in the `requirements.txt` file.
